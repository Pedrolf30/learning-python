available_toppings=['extra cheese','mushrooms','olives']
requested_toppings=['mushrooms','extra cheese','french fries']

for requested_topping in requested_toppings:
    if requested_topping in available_toppings:
        print("Adding "+requested_topping)
    else:
        print("Sorry we are out of "+requested_topping)
