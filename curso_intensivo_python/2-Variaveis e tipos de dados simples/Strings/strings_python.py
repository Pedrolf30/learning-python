msg_1 = "This is a string"
msg_2 = 'This is also a string'


#Mudar letras maiusculas e minisculas usando python

#Colocar somente a primeira letra das palavras em maiusculo
nome = "ada lovace"
print(nome.title())

#Colocar somente em letras maiusculas
nome = "ada lovace"
print(nome.upper())

#Colocar somente em letras minusculas
nome = "ADA LOVACE"
print(nome.lower())