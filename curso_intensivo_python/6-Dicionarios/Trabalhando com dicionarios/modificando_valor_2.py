alien={"x_position":"0","y_position":"25","speed":"medium"}
print("\n Original x_position: "+str(alien['x_position']))
#Mover alien para a direita
#Determinar a velocidade

if alien['speed']=='slow':
    x_increment=1
elif alien['speed']=='medium':
    x_increment=2
else:
    x_increment=3

alien['x_position'] = int(alien['x_position']) + int(x_increment)
print("\n\n The new alien position is: "+str(alien['x_position']))