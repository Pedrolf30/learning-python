#Cria uma lista vazia para armazenar os aliens
aliens=[]
#Cria 30 aliens verdes
for alien_number in range(30):
    new_alien={"color":"green","points":5,"speed":"slow"}
    aliens.append(new_alien)
#Mostra os 5 primeiros aliens
for alien in aliens[:5]:
    print(alien)
#Mostra quantos aliens foram criados
print("\n Total number of aliens: "+str(len(aliens)))