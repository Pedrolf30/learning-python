#Armazena informacoes de uma pizza que esta sendo pedida
pizza={"crust":"thick","toppings":['mushrooms','extra cheese']}
#Resume o pedido
print("You ordered a "+pizza['crust']+"-crust pizza, with the following toppings: ")
for topping in pizza['toppings']:
    print(topping)