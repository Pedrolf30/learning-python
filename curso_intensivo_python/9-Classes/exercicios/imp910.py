class Restaurante():
    def __init__(self,nome,tipo):
        self.nome=nome
        self.tipo=tipo
    def describe(self):
        print("\n The restaurant name is: "+self.nome.title()+" and it servers: "+self.tipo)
    def openzada(self):
        print("\n "+self.nome.title()+" is open now")
class IceCreamStand(Restaurante):
    def __init__(self,nome,tipo):
        super().__init__(nome,tipo)
    def flavors(self):
        print("Alem de no "+self.nome.title()+" servir "+self.tipo.title())
        print("Temos tambem um stand de sorvete, no qual temos disponiveis os seguintes sabores: ")
        list_flavors = ['chocolate', 'morango', 'baunilha']
        for flavor in list_flavors:
            print("-"+flavor)