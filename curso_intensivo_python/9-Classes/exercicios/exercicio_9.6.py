#Crie uma classe chamada restaurante. Deve armazenar o nome e o tipo de comida, com 2 metodos um que descreve o restaurante e um que diga q ele esta aberto
class Restaurante():
    def __init__(self,nome,tipo):
        self.nome=nome
        self.tipo=tipo
    def describe(self):
        print("\n The restaurant name is: "+self.nome.title()+" and it servers: "+self.tipo)
    def openzada(self):
        print("\n "+self.nome.title()+" is open now")
class IceCreamStand(Restaurante):
    def __init__(self,nome,tipo):
        super().__init__(nome,tipo)
    def flavors(self):
        print("Alem de no "+self.nome.title()+" servir "+self.tipo.title())
        print("Temos tambem um stand de sorvete, no qual temos disponiveis os seguintes sabores: ")
        for flavor in list_flavors:
            print("-"+flavor)

restaurante=IceCreamStand("Herobino","Italian food")
restaurante.describe()
restaurante.openzada()
list_flavors=['chocolate','morango','baunilha']
restaurante.flavors()