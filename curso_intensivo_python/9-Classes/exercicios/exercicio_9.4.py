#Comece com seu programa do exercicio 9.1. Acrescente um atributo chamado number_served cujo valor default eh 0.
# Apresente o numero de clientes atendidos pelo restaurante, em seguida mude isso e exiba denovo.
class Res():
    def __init__(self,nome,tipo):
        self.nome=nome
        self.tipo=tipo
        self.number_served=0
    def describe(self):
        print("\n Te restaurant name is: "+self.nome.title())
        print("\n And it serves: "+self.tipo.title())
    def openzada(self):
        print("\n "+self.nome.title()+" is open now")
    def read_served(self):
        print("\n We`ve served "+str(self.number_served)+" clients")
    def update_served(self,number):
        self.number_served=number

res=Res("biroliro","arabian food")
res.describe()
res.openzada()
res.read_served()
res.update_served(12)
res.read_served()