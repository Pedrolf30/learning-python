class Car():
    def __init__(self,make,model,year):
        self.make=make
        self.model=model
        self.year=year
        self.odometer_reading=0
    def descritive_name(self):
        long_name = (str(self.year) + " " + self.make.title() + " " + self.model.title())
        return long_name
    def read_odometer(self):
        print("This car has "+str(self.odometer_reading)+" miles on it")
    def update_odometer(self,mileage):
        self.odometer_reading=mileage
    def increment_odometer(self,miles):
        self.odometer_reading+=miles
class EletricCar(Car):
    def __init__(self,make,model,year):
        super().__init__(make,model,year)

my_car=EletricCar('tesla',"s",2016)
my_car.read_odometer()
print(my_car.descritive_name())
