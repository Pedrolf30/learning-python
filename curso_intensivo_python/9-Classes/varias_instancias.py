class Dog():
    def __init__(self,name,age):
        self.name=name
        self.age=age
    def sit(self):
        print(self.name.title()+"is now sitting")
    def roll_over(self):
        print(self.name.title()+"is rolling over")
my_dog=Dog('willie',6)
print("My dog name is: "+my_dog.name.title())
print(my_dog.name.title()+" age is: "+str(my_dog.age))
your_dog=Dog('clay',3)
print("Your dog name is: "+your_dog.name.title())
print(your_dog.name.title()+" age is: "+str(your_dog.age))