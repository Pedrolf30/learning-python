class Car():
    def __init__(self,make,model,year):
        self.make=make
        self.model=model
        self.year=year
        self.odometer_reading=0
    def descritive_name(self):
        long_name = (str(self.year) + " " + self.make.title() + " " + self.model.title())
        return long_name
    def read_odometer(self):
        print("This car has "+str(self.odometer_reading)+" miles on it")
    def update_odometer(self,mileage):
        self.odometer_reading=mileage
    def increment_odometer(self,miles):
        self.odometer_reading+=miles
class Battery():
    def __init__(self,battery_size=70):
        self.battery_size=battery_size
    def describe_battery(self):
        print("\n This car has a " + str(self.battery_size) + " KWh battery")
    def get_range(self):
        if self.battery_size==70:
            range=240
        elif self.battery_size==85:
            range=270
        msg="\n This car can go approxinately "+str(range)
        msg+=" Miles on full charge"
        print(msg)
class EletricCar(Car):
    def __init__(self,make,model,year):
        super().__init__(make,model,year)
        self.battery=Battery()
