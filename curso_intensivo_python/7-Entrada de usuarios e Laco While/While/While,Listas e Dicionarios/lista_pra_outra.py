#Comeca com os usuarios a serem verificados
#Uma lista vazia e criada para armazenar os usuarios confirmados
unconfirmed=['pedro','marcos','tiago','samuel']
confirmed=[]
#Verifica um por um ate que nao haja mais unconfirmeds
#Transfere os verificados para os confirmados
while unconfirmed:
    verificando=unconfirmed.pop()
    print("Verifying: "+verificando.title())
    confirmed.append(verificando)
    print(verificando.title()+", foi verificado com sucesso")
#Exibe todos os usuario confirmados
print("\n The following users are already confirmed: ")
for user in confirmed:
    print(user)