prompt="\n Tell me something and Ill repeat back for you"
prompt+="\n Enter 'quit' to end the program: "
active=True
while active:
    msg=input(prompt)

    if msg == 'quit':
        active=False
    else:
        print(msg)