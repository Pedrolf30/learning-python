#Use uma instrucao while, com uma variavel active e uma instrucao break para sair ao digitar quit
prompt="\n Tell me something and Ill repeat back for you"
prompt+="\n Enter 'quit' to end the program: "
active=True
count=0
while active:
    msg=input(prompt)
    count+=1
    if msg != 'quit':
        print(msg)
    if msg == 'quit':
        break
    if count>=5:
        active=False