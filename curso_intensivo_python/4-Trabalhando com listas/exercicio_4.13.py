#Pense em 5 pratos de comida e os armazena em uma tupla
#Use o laco for para os exibir
#Sobrescreva o menu, e utilize for para exibir o menu revisado

pratos=('lasanha','strongonoff','parmegiana')
print('Original menu:')
for prato in pratos:
    print(prato)

print('\nNew menu:')
pratos=('racao de cachorro','alpiste','racao de gato')
for prato in pratos:
    print(prato)