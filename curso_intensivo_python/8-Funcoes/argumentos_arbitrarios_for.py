def make_pizza(*toppings):
    print("\n Making a pizza with the following toppings:")
    for topping in toppings:
        print("-"+topping)

make_pizza('extra cheese','mushrooms','green peppers')