def make_pizza(size,*toppings):
    print("\n Im making a "+str(size)+"-inches pizza with the following toppings:")
    for topping in toppings:
        print("-"+topping)

make_pizza(12,'mushrooms','extra cheese','pepperoni')